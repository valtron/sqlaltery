# SQLAltery

A migration library for SQLAlchemy. A cleaner frontend to Alembic.

## Status

- [x] `MetaData` diffing (`sqlaltery/compare.py`)
- [x] apply ops to `MetaData` (`sqlaltery/ops.py:apply`)
- [x] apply ops to `Connection` (`sqlaltery/ops.py:apply`); all the heavy lifting is passed to Alembic's DDL
- [x] write migration to a file (`sqlaltery/command.py:_save_migration`)
- [ ] tests (95%)
- [ ] docs

## Why not Alembic?

- Alembic diffs the head MD against current DB, not against latest migration
- Alembic has split upgrade/downgrade
- Alembic requires you to manually manage metadata in migrations (for doing queries)
- Alembic creates a bunch of files (alembic.ini, env.py). The default case should be simple: just create an `SQLAltery` and use it; customize by passing arguments.
