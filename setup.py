from pathlib import Path
import setuptools

setuptools.setup(
	name = 'sqlaltery',
	version = '0.4.0',
	author = "valtron",
	description = "A migration library for SQLAlchemy.",
	long_description = Path('README.md').read_text(),
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/sqlaltery',
	packages = ['sqlaltery'],
	python_requires = '>=3.8',
	install_requires = ['sqlalchemy ~= 2.0.35', 'alembic ~= 1.13.3'],
	extras_require = {
		'test': [
			'pytest', 'pytest-cov', 'duckdb-engine',
		],
	},
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.8',
		'Programming Language :: Python :: 3.9',
		'Programming Language :: Python :: 3.10',
		'Programming Language :: Python :: 3.11',
		'Programming Language :: Python :: 3.12',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)
