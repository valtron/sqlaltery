from sqlaltery import ops
import sqlalchemy as sa

OPS = [
	ops.AddTable('table', (
		sa.Column('col1', sa.String(length=30), server_default='abc', nullable=False),
		sa.Column('col2', sa.Integer()),
	)),
	ops.DropTable('table'),
	ops.AlterTable('table', name='new_name'),
	ops.AddColumn('table', sa.Column('col1', sa.Integer())),
	ops.DropColumn('table', 'col1'),
	ops.AlterColumn('table', 'col1', type=sa.String(length=10)),
	ops.AddPrimaryKey('table', ('id',)),
	ops.DropPrimaryKey('table'),
	ops.AddForeignKey('table', ('id', 'foo'), 'other_table', ('id', 'bar'), name='fk_name'),
	ops.DropForeignKey('table', 'fk_name'),
	ops.AddUnique('table', ('col1', 'col2'), deferrable=True),
	ops.DropUnique('table', 'uq_name'),
	ops.AddCheck('table', '1+1=2', name='ck_name'),
	ops.DropCheck('table', 'ck_name'),
	ops.AddIndex('table', ('col1',), unique=True),
	ops.DropIndex('table', 'ix_name'),
]
