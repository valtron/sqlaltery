import sqlalchemy as sa
from sqlaltery import ops

OPS = [
	ops.AddTable('table1', [
		sa.Column('id', sa.Integer()),
	]),
	ops.AlterTable('table1', name = 'tableX'),
	ops.AddColumn('tableX', sa.Column('foo', sa.String(30), nullable = False, server_default = '')),
]
