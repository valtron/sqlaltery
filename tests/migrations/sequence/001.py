from sqlaltery import ops
import sqlalchemy as sa

OPS = [
	ops.AddTable('table1', (
		sa.Column('id', sa.Integer(), server_default=sa.sql.functions.next_value(sa.Sequence('table1_id_seq', start=2, increment=3)), primary_key=True, nullable=False),
		sa.Column('txt', sa.String(length=30), nullable=False),
	)),
]
