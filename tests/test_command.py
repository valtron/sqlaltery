from typing import Optional, List
from pathlib import Path
import shutil

import pytest
import sqlalchemy as sa

from sqlaltery import SQLAltery, command, ops
from .conftest import get_dbs

@pytest.mark.parametrize('revision', [None, 1])
def test_push(revision: Optional[int], tmp_path: Path) -> None:
	d = migration_dir(tmp_path, 'tests/migrations/basic')
	altery = SQLAltery(d)
	engine = sa.create_engine('sqlite:///:memory:', echo = False)
	with altery.connect_to(engine) as migrator:
		assert migrator.get_db_revision() is None
		migrator.migrate(revision, initial = 0)
		assert migrator.get_db_revision() == 1
	md = _md_001()
	tx = md.tables['tableX']
	with engine.begin() as conn:
		conn.execute(tx.insert().values([{ 'id': 1, 'foo': 'bar' }]))
		rows = conn.execute(sa.select(tx.c['id'], tx.c['foo'])).fetchall()
		assert rows == [(1, 'bar')]
	with altery.connect_to(engine) as migrator:
		assert migrator.get_db_revision() == 1
		migrator.migrate(0, initial = 0)
		assert migrator.get_db_revision() == 0

@pytest.mark.parametrize('conn_str', get_dbs())
def test_real_db(tmp_path: Path, conn_str: str) -> None:
	d = migration_dir(tmp_path, 'tests/migrations/basic')
	altery = SQLAltery(d)
	engine = sa.create_engine(conn_str, echo = False)
	with altery.connect_to(engine) as migrator:
		assert migrator.get_db_revision() is None
		migrator.migrate(1, initial = 0)
		assert migrator.get_db_revision() == 1
	md = _md_001()
	tx = md.tables['tableX']
	with engine.begin() as conn:
		conn.execute(tx.insert().values([{ 'id': 1, 'foo': 'bar' }]))
		rows = conn.execute(sa.select(tx.c['id'], tx.c['foo'])).fetchall()
		assert rows == [(1, 'bar')]
	with altery.connect_to(engine) as migrator:
		assert migrator.get_db_revision() == 1
		migrator.migrate(0, initial = 0)
		assert migrator.get_db_revision() == 0

def test_commit(tmp_path: Path) -> None:
	d = migration_dir(tmp_path, 'unused')
	altery = SQLAltery(d)
	altery._migrations = [command.Migration(1, [
		ops.AddTable('table1', [
			sa.Column('id', sa.Integer()),
		]),
		ops.AlterTable('table1', name = 'tableX'),
		ops.AddColumn('tableX', sa.Column('foo', sa.String(30))),
	])]
	diff_ops = altery._diff(_md_002())
	assert diff_ops == _a_diff()
	assert not d.exists()

def test_nonexistant_directory(tmp_path: Path) -> None:
	d = tmp_path / 'migrations'
	altery = SQLAltery(d)
	altery.generate(_md_001())
	assert (d / '001.py').exists()

def test_generate_empty(tmp_path: Path) -> None:
	d = migration_dir(tmp_path, 'tests/migrations/basic')
	altery = SQLAltery(d)
	altery.generate(_md_001())
	filename = (d / '002.py')
	assert not filename.exists()

def test_generate_nonempty(tmp_path: Path) -> None:
	d = migration_dir(tmp_path, 'tests/migrations/basic')
	altery = SQLAltery(d)
	altery.generate(_md_002())
	text = (d / '002.py').read_text()
	scope = {}
	exec(text, scope, scope)
	ops = scope['OPS']
	assert ops == _a_diff()

def test_sequence_pk_generate(tmp_path: Path) -> None:
	d = tmp_path
	altery = SQLAltery(d)
	altery.generate(_md_sequence_pk())
	assert 'Sequence(' in (d / '001.py').read_text()

def test_sequence_pk_migrate(tmp_path: Path) -> None:
	d = migration_dir(tmp_path, 'tests/migrations/sequence')
	altery = SQLAltery(d)
	engine = sa.create_engine('duckdb:///:memory:', echo = False)
	with altery.connect_to(engine) as migrator:
		assert migrator.get_db_revision() is None
		migrator.migrate(1, initial = 0)
		assert migrator.get_db_revision() == 1
	with engine.begin() as conn:
		conn.execute(sa.text("insert into table1(txt) values('a'),('b')"))
		rows = conn.execute(sa.text("select id, txt from table1")).all()
	assert rows == [
		(2, 'a'),
		(5, 'b'),
	]
	with altery.connect_to(engine) as migrator:
		migrator.migrate(0, initial = 0)
		assert migrator.get_db_revision() == 0
	with engine.begin() as conn:
		with pytest.raises(sa.exc.ProgrammingError):
			conn.execute(sa.text("select nextval('\"table1_id_seq\"')"))

def migration_dir(tmp_path: Path, orig_dir: str) -> Path:
	d = Path(orig_dir)
	dst = tmp_path / d.name
	if d.exists():
		shutil.copytree(str(d), str(dst))
	return dst

def _md_001() -> sa.MetaData:
	md = sa.MetaData()
	sa.Table('tableX', md,
		sa.Column('id', sa.Integer()),
		sa.Column('foo', sa.String(30), nullable = False, server_default = ''),
	)
	return md

def _md_002() -> sa.MetaData:
	md = sa.MetaData()
	sa.Table('table1', md,
		sa.Column('id', sa.Integer, primary_key = True),
		sa.Column('foo', sa.String(30)),
		sa.Column('bar', sa.Boolean, nullable = False),
	)
	sa.Table('table2', md,
		sa.Column('id', sa.Integer, primary_key = True),
		sa.Column('t1_id', sa.Integer, sa.ForeignKey('table1.id')),
		sa.Column('zim', sa.String(20), nullable = False),
	)
	return md

def _md_sequence_pk() -> sa.MetaData:
	md = sa.MetaData()
	id_seq = sa.Sequence('table1_id_seq', start = 2, increment = 3)
	sa.Table('table1', md,
		sa.Column('id', sa.Integer, primary_key = True, server_default = id_seq.next_value()),
		sa.Column('txt', sa.String(30), nullable = False),
	)
	return md

def _a_diff() -> List[ops.Operation]:
	return [
		ops.DropPrimaryKey('tableX'),
		ops.DropTable('tableX'),
		ops.AddTable('table1', [
			sa.Column('bar', sa.Boolean(), nullable = False),
			sa.Column('foo', sa.String(30)),
			sa.Column('id', sa.Integer(), nullable = False),
		]),
		ops.AddTable('table2', [
			sa.Column('id', sa.Integer(), nullable = False),
			sa.Column('t1_id', sa.Integer()),
			sa.Column('zim', sa.String(20), nullable = False),
		]),
		ops.AddPrimaryKey('table1', ('id',)),
		ops.AddPrimaryKey('table2', ('id',)),
		ops.AddForeignKey('table2', ('t1_id',), 'table1', ('id',)),
	]
