from typing import List

import sqlalchemy as sa

from sqlaltery.compare import diff_md
from sqlaltery import ops

def test_1() -> None:
	md0 = meta_0()
	assertDiffEquals(md0, meta_0(), [])
	
	md1 = meta_1()
	assertDiffEquals(md1, meta_1(), [])
	
	assertDiffEquals(md0, md1, [
		ops.AddTable('book', [
			sa.Column('title', sa.String(30), nullable = False, primary_key = True),
			sa.Column('author', sa.String(30)),
		]),
		ops.AddPrimaryKey('book', ('title',)),
	])
	
	md2 = meta_2()
	assertDiffEquals(md2, meta_2(), [])
	
	assertDiffEquals(md1, md2, [
		ops.DropPrimaryKey('book'),
		ops.AddColumn('book', sa.Column('id', sa.Integer(), nullable = False)),
		ops.AddPrimaryKey('book', ('id',)),
		ops.AlterColumn('book', 'author', nullable = False),
	])
	
	md3 = meta_3()
	assertDiffEquals(md3, meta_3(), [])
	
	assertDiffEquals(md2, md3, [
		ops.AddTable('author', [
			sa.Column('id', sa.Integer(), nullable = False),
			sa.Column('name', sa.String(100), nullable = False),
			sa.Column('is_pseudonym', sa.Boolean(), nullable = False),
		]),
		ops.AddIndex('author', ('name',), name = 'ix_author_name'),
		ops.AddPrimaryKey('author', ('id',)),
		ops.AddUnique('author', ('name', 'is_pseudonym')),
		ops.AddForeignKey('book', ('author',), 'author', ('id',)),
		ops.AddCheck('author', 'name <> "Steve"'),
		ops.AlterColumn('book', 'author', type = sa.Integer()),
		ops.AlterColumn('book', 'title', type = sa.String(45)),
	])

def test_rev() -> None:
	md0 = meta_0()
	md3 = meta_3()
	
	changes = [
		ops.DropCheck('author', None),
		ops.DropForeignKey('book', None),
		ops.DropUnique('author', None),
		ops.DropPrimaryKey('author'),
		ops.DropPrimaryKey('book'),
		ops.DropIndex('author', 'ix_author_name'),
		ops.DropTable('author'),
		ops.DropTable('book'),
	]
	
	assertDiffEquals(md3, md0, changes)
	
	md = meta_3()
	for op in changes:
		op.generate_reverse(md)
	
	changes_reverse = [
		ops.AddCheck('author', 'name <> "Steve"'),
		ops.AddForeignKey('book', ('author',), 'author', ('id',)),
		ops.AddUnique('author', ('name', 'is_pseudonym')),
		ops.AddPrimaryKey('author', ('id',)),
		ops.AddPrimaryKey('book', ('id',)),
		ops.AddIndex('author', ('name',), name = 'ix_author_name'),
		ops.AddTable('author', [
			sa.Column('id', sa.Integer(), primary_key=True, nullable = False),
			sa.Column('is_pseudonym', sa.Boolean(), nullable = False),
			sa.Column('name', sa.String(length = 100), nullable = False)
		]),
		ops.AddTable('book', [
			sa.Column('author', sa.Integer(), nullable = False),
			sa.Column('id', sa.Integer(), primary_key = True, nullable = False),
			sa.Column('title', sa.String(length = 45), nullable = False)
		]),
	]
	assert [op.reverse for op in changes] == changes_reverse
	
	# Diffing always orders partly alphabetically,
	# so it's not a simple `reversed(changes_reverse)`
	assertDiffEquals(md0, md3, list(reversed([changes_reverse[i] for i in [
		0, 1, 2, 4, 3, 5, 7, 6
	]])))

def test_compare() -> None:
	op1 = ops.AddColumn('table', sa.Column('foo', sa.String(30)))
	op2 = ops.AddColumn('table', sa.Column('foo', sa.String(30)))
	assert op1 == op2
	
	op1 = ops.AlterColumn('book', 'author', type = sa.Integer())
	op2 = ops.AlterColumn('book', 'author', type = sa.Integer())
	assert op1 == op2
	
	c1 = sa.CheckConstraint('a <> b')
	t1 = sa.Table('t', meta_0(), c1)
	c2 = sa.CheckConstraint('a <> b')
	t2 = sa.Table('t', meta_0(), c2)
	op1 = ops.AddCheck.FromSchemaObject(c1)
	op2 = ops.AddCheck.FromSchemaObject(c2)
	assert op1 == op2
	
	op1 = ops.AlterTable('table_y', name = 'table_x')
	op2 = ops.AlterTable('table_x', name = 'table_x')
	assert op1 != op2

def test_rev2() -> None:
	changes = [
		ops.AddTable('table_x', []),
		ops.AlterTable('table_x', name = 'table_y'),
	]
	md = meta_0()
	for op in changes:
		op.generate_reverse(md)
	assert [op.reverse for op in changes] == [
		ops.DropTable('table_x'),
		ops.AlterTable('table_y', name = 'table_x'),
	]

def test_misc() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1, sa.Column('c', sa.Integer))
	md2 = sa.MetaData()
	sa.Table('t', md2, sa.Column('d', sa.Integer))
	assertDiffEquals(md1, md2, [
		ops.DropColumn('t', 'c'),
		ops.AddColumn('t', sa.Column('d', sa.Integer)),
	])

def test_alter_column() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1, sa.Column('c', sa.Integer))
	md3 = sa.MetaData()
	sa.Table('t', md3, sa.Column('c', sa.String(30), nullable = False, server_default = 'x', server_onupdate = 'y'))
	assertDiffEquals(md1, md3, [
		ops.AlterColumn('t', 'c', type = sa.String(30), nullable = False, server_default = 'x', server_onupdate = 'y'),
	])

def test_alter_pk() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1,
		sa.Column('c', sa.Integer, nullable = False),
		sa.Column('d', sa.Integer, nullable = False),
		sa.PrimaryKeyConstraint('c', name = 'pk_t'),
	)
	
	md2 = sa.MetaData()
	sa.Table('t', md2,
		sa.Column('c', sa.Integer, nullable = False),
		sa.Column('d', sa.Integer, nullable = False),
		sa.PrimaryKeyConstraint('c', 'd', name = 'pk_t', deferrable = True, initially = 'foo'),
	)
	
	assertDiffEquals(md1, md2, [
		ops.DropPrimaryKey('t', 'pk_t'),
		ops.AddPrimaryKey('t', ('c', 'd'), name = 'pk_t', deferrable = True, initially = 'foo'),
	])

def test_alter_uq() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.UniqueConstraint('c', name = 'uq_t'),
	)
	
	md2 = sa.MetaData()
	sa.Table('t', md2,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.UniqueConstraint('c', 'd', name = 'uq_t', deferrable = True, initially = 'foo'),
	)
	
	assertDiffEquals(md1, md2, [
		ops.DropUnique('t', 'uq_t'),
		ops.AddUnique('t', ('c', 'd'), name = 'uq_t', deferrable = True, initially = 'foo'),
	])

def test_alter_ix() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.Index('ix_t', 'c'),
	)
	
	md2 = sa.MetaData()
	sa.Table('t', md2,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.Index('ix_t', 'c', 'd', unique = True),
	)
	
	assertDiffEquals(md1, md2, [
		ops.DropIndex('t', 'ix_t'),
		ops.AddIndex('t', ('c', 'd'), name = 'ix_t', unique = True),
	])

def test_alter_fk() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.ForeignKeyConstraint(('c',), ('f.e',), name = 'fk_t'),
	)
	sa.Table('f', md1,
		sa.Column('e', sa.Integer),
		sa.Column('f', sa.Integer),
	)
	sa.Table('g', md1,
		sa.Column('e', sa.Integer),
		sa.Column('f', sa.Integer),
	)
	
	md2 = sa.MetaData()
	sa.Table('t', md2,
		sa.Column('c', sa.Integer),
		sa.Column('d', sa.Integer),
		sa.ForeignKeyConstraint(
			('c', 'd'), ('g.e', 'g.f'), name = 'fk_t',
			onupdate = 'CASCADE', ondelete = 'RESTRICT', deferrable = True,
			initially = 'foo', match = 'PARTIAL',
		),
	)
	sa.Table('f', md2,
		sa.Column('e', sa.Integer),
		sa.Column('f', sa.Integer),
	)
	sa.Table('g', md2,
		sa.Column('e', sa.Integer),
		sa.Column('f', sa.Integer),
	)
	
	assertDiffEquals(md1, md2, [
		ops.DropForeignKey('t', 'fk_t'),
		ops.AddForeignKey(
			't', ('c', 'd'), 'g', ('e', 'f'), name = 'fk_t',
			onupdate = 'CASCADE', ondelete = 'RESTRICT', deferrable = True,
			initially = 'foo', match = 'PARTIAL',
		),
	])

def test_alter_ck() -> None:
	md1 = sa.MetaData()
	sa.Table('t', md1,
		sa.Column('c', sa.Integer),
		sa.CheckConstraint('c > 0', name = 'ck_t'),
	)
	
	md2 = sa.MetaData()
	sa.Table('t', md2,
		sa.Column('c', sa.Integer),
		sa.CheckConstraint('c < 0', name = 'ck_t', deferrable = True, initially = 'foo'),
	)
	
	assertDiffEquals(md1, md2, [
		ops.DropCheck('t', 'ck_t'),
		ops.AddCheck('t', 'c < 0', name = 'ck_t', deferrable = True, initially = 'foo'),
	])

def meta_0() -> sa.MetaData:
	return sa.MetaData()

def meta_1() -> sa.MetaData:
	md = sa.MetaData()
	sa.Table('book', md,
		sa.Column('title', sa.String(30), primary_key = True),
		sa.Column('author', sa.String(30)),
	)
	return md

def meta_2() -> sa.MetaData:
	md = sa.MetaData()
	sa.Table('book', md,
		sa.Column('id', sa.Integer, primary_key = True),
		sa.Column('title', sa.String(30), nullable = False),
		sa.Column('author', sa.String(30), nullable = False),
	)
	return md

def meta_3() -> sa.MetaData:
	md = sa.MetaData()
	sa.Table('book', md,
		sa.Column('id', sa.Integer, primary_key = True),
		sa.Column('title', sa.String(45), nullable = False),
		sa.Column('author', sa.Integer, sa.ForeignKey('author.id'), nullable = False),
	)
	sa.Table('author', md,
		sa.Column('id', sa.Integer, primary_key = True),
		sa.Column('name', sa.String(100), nullable = False, index = True),
		sa.Column('is_pseudonym', sa.Boolean, nullable = False),
		sa.UniqueConstraint('name', 'is_pseudonym'),
		sa.CheckConstraint('name <> "Steve"'),
	)
	return md

def assertDiffEquals(md_old: sa.MetaData, md_new: sa.MetaData, operations: List[ops.Operation]) -> None:
	assert diff_md(md_old, md_new) == operations
