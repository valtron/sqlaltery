from pathlib import Path
import io

from sqlaltery import render
from sqlaltery.migration import Migration

def test_empty() -> None:
	assertRoundtrip('tests/render/empty.py')

def test_nonempty() -> None:
	assertRoundtrip('tests/render/nonempty.py')

def assertRoundtrip(file):
	text = Path(file).read_text()
	scope = {}
	exec(text, scope, scope)
	m = Migration(1, scope['OPS'])
	buf = io.StringIO()
	render.render_migration(m, buf)
	assert buf.getvalue() == text
