import pytest
import sqlalchemy as sa

from sqlaltery import ops
from .conftest import get_dbs

def test_reverse() -> None:
	md = sa.MetaData()
	sa.Table('f', md, sa.Column('g', sa.String(10)))
	
	operations = [
		ops.AddTable('t1', []),
		ops.AlterTable('t1', name = 't'),
		
		ops.AddColumn('t', sa.Column('c1', sa.Integer())),
		ops.AlterColumn('t', 'c1', name = 'c', type = sa.String(10), nullable = False, server_default = 'x', server_onupdate = 'y'),
		
		ops.AddPrimaryKey('t', ('c',)),
		ops.DropPrimaryKey('t'),
		
		ops.AddForeignKey('t', ('c',), 'f', ('g',), name = 'fk'),
		ops.DropForeignKey('t', 'fk'),
		
		ops.AddUnique('t', ('c',), name = 'uq'),
		ops.DropUnique('t', 'uq'),
		
		ops.AddCheck('t', 'c > 0', name = 'ck'),
		ops.DropCheck('t', 'ck'),
		
		ops.AddIndex('t', ('c',), name = 'ix'),
		ops.DropIndex('t', 'ix'),
		
		ops.DropColumn('t', 'c'),
		ops.DropTable('t'),
	]
	for op in operations:
		op.generate_reverse(md)

@pytest.mark.parametrize('conn_str', get_dbs())
def test_apply(conn_str: str) -> None:
	md = sa.MetaData()
	sa.Table('table1', md,
		sa.Column('col1', sa.Integer()),
		sa.Column('col2', sa.String(10)),
		sa.Column('col3', sa.Boolean()),
	)
	
	engine = sa.create_engine(conn_str, echo = False)
	md.create_all(engine)
	
	def apply(op):
		op.apply(md, conn)
	
	is_mysql = conn_str.startswith('mysql')
	
	with engine.connect() as conn:
		apply(ops.DataOperation(lambda md, conn: (
			conn.execute(md.tables['table1'].insert().values([
				{ 'col1': i, 'col2': 's{}'.format(i), 'col3': i<5 }
				for i in range(10)
			]))
		)))
		
		apply(ops.AlterTable('table1', name = 't'))
		assert 'table1' not in md.tables
		assert 't' in md.tables
		conn.execute(sa.select(md.tables['t'])).fetchall()
		
		apply(ops.AlterColumn('t', 'col1', name = 'c'))
		assert 'col1' not in md.tables['t'].c
		assert 'c' in md.tables['t'].c
		conn.execute(sa.select(md.tables['t'].c['c'])).fetchall()
		
		q = sa.select(md.tables['t'].c['col3']).compile(conn)
		
		apply(ops.DropColumn('t', 'col3'))
		assert 'col3' not in md.tables['t'].c
		with pytest.raises(Exception):
			conn.execute(q)
		
		apply(ops.DataOperation(md.tables['t'].insert().values([
			{ 'c': 0, 'col2': 'dupe' }
		])))
		with pytest.raises(sa.exc.IntegrityError):
			apply(ops.AddPrimaryKey('t', ('c',), name = 'pk_t'))
		try:
			# TODO: Hack; only applies to SQLite
			conn.execute(sa.text('DROP TABLE _alembic_tmp_t'))
		except Exception:
			if not conn_str.startswith('sqlite'):
				raise
		conn.execute(md.tables['t'].delete().where(md.tables['t'].c['col2'] == 'dupe'))
		apply(ops.AddPrimaryKey('t', ('c',), name = 'pk_t'))
		with pytest.raises(Exception):
			conn.execute(md.tables['t'].insert().values([
				{ 'c': 0, 'col2': 'dupe' }
			]))
		
		# TODO: Queries. A lot of these could pass even if their implementation was no-ops.
		apply(ops.DropPrimaryKey('t', 'pk_t'))
		apply(ops.AddTable('t2', [sa.Column('c1', sa.Integer(), nullable = False)]))
		apply(ops.AddUnique('t2', ('c1',), name = 'uq_test'))
		
		conn.execute(md.tables['t2'].insert().values([
			{ 'c1': i } for i in range(10)
		]))
		
		apply(ops.AddForeignKey('t', ('c',), 't2', ('c1',), name = 'fk_test'))
		apply(ops.DropForeignKey('t', 'fk_test'))
		apply(ops.DropUnique('t2', 'uq_test'))
		apply(ops.DropTable('t2'))
		if not is_mysql:
			apply(ops.AddCheck('t', 'c >= 0', name = 'ck_test'))
			apply(ops.DropCheck('t', 'ck_test'))
		apply(ops.AddIndex('t', ('c', 'col2'), name = 'ix_test', unique = True))
		apply(ops.DropIndex('t', 'ix_test'))

def test_hash() -> None:
	o1 = ops.AddColumn('t', sa.Column('c', sa.String(30), nullable = True))
	o2 = ops.AddColumn('t', sa.Column('c', sa.String(30)))
	o3 = ops.AddColumn('t', sa.Column('c', sa.String(29)))
	assert hash(o1) == hash(o2)
	assert o1 == o2
	assert o2 != o3
	assert o1 != o3

def test_data_rev() -> None:
	fwd = lambda md, conn: None
	bwd = lambda md, conn: None
	op = ops.DataOperation(fwd, bwd)
	assert op.forwards is fwd
	assert op.backwards is bwd
	op.generate_reverse(sa.MetaData())
	rop = op.reverse
	assert rop.forwards is bwd
	assert rop.backwards is fwd

def test_unimportant() -> None:
	md = sa.MetaData()
	sa.Table('t', md, sa.Column('c', sa.Integer))
	op = ops.AlterColumn('t', 'c')
	op.apply(md, None)
