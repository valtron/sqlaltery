from typing import List
import os

def get_dbs() -> List[str]:
	dbs = os.environ.get('REAL_DBS')
	return ['sqlite:///:memory:', 'duckdb:///:memory:'] + (dbs.split(',') if dbs else [])
